//
//  File.swift
//  Struct and Class
//
//  Created by Audhy Virabri Kressa on 28/04/20.
//  Copyright © 2020 Audhy Virabri Kressa. All rights reserved.
//

import Foundation

enum CompassPoint {
    case north, south, east, west
    mutating func turnNorth() {
        self = .north
    }
}
