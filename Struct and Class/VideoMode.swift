//
//  VideoMode.swift
//  Struct and Class
//
//  Created by Audhy Virabri Kressa on 28/04/20.
//  Copyright © 2020 Audhy Virabri Kressa. All rights reserved.
//

import Foundation

class VideoMode {
    var resolution = Resolution()
    var interlaced = false
    var frameRate = 0.0
    var name: String?
}
