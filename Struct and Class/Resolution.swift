//
//  Resolution.swift
//  Struct and Class
//
//  Created by Audhy Virabri Kressa on 28/04/20.
//  Copyright © 2020 Audhy Virabri Kressa. All rights reserved.
//

import Foundation

/**
    Ini Struct Resolutuon
 */

//https:developer.apple.com/library/archive/documentation/Xcode/Reference/xcode_markup_formatting_ref/ComentBlock.html

struct Resolution {
    var width = 0
    var height = 0
}
